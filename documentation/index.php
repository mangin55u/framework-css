<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Easy : Documentation</title>
    <link rel="icon" type="image/png" href="../images/logo_framework.png"/>
    <link rel="stylesheet" type="text/css" href="../css/documentation.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<div id="container">
    <div id="started">
        <div class="header">
            <h3>Get Started</h3>
        </div>

        <p><em>To use Easy, it is simple, you only need to download the CSS framework and import it into your HTML
            </em></p>

                <pre class="example-pre">
                <?php
                $html = '
<link rel="stylesheet" type="text/css" href="css/easy.css">
                ';
                echo htmlspecialchars($html);
                ?>
            </pre>
    </div>

    <div id="navigation">
        <div class="header">
            <h3>Navigation</h3>
        </div>

        <p><em>Pagination system, navigation bar, and breadcumb to find your way</em></p>

        <div class="typo">
            <strong>Pagination</strong>

            <div class="pagination">
                <ul>
                    <li>1</li>
                    <li class="active">2</li>
                    <li>3</li>
                    <li class="disabled">4</li>
                </ul>
            </div>
        </div>

        <div class="typo">
            <strong>Navigation bar</strong>

            <div id="menu">
                <ul>
                    <li class="active"><a href="#typography">Typography</a></li>
                    <li><a href="#tables">Tables</a></li>
                    <li><a href="#buttons">Buttons</a></li>
                    <li class="disabled"><a href="#">Boxes</a></li>
                </ul>
            </div>
        </div>

        <div class="typo">
            <strong>Breadcrumb</strong>

            <div id="fil">
                <ul>
                    <li><a href="#typography">Typography</a></li>
                    <li><a href="#tables">Tables</a></li>
                    <li><a href="#buttons">Buttons</a></li>
                    <li><a href="#">Boxes</a></li>
                </ul>
            </div>
        </div>

        <pre class="example-pre">
                <?php
                $html = '
/* Pagination */
<div class="pagination">
    <ul>
        <li>...</li>
        <li class="active">...</li>
        <li class="disabled">...</li>
    </ul>
</div>


/* Navigation Bar */
<div class="navbar">
    <ul>
        <li class="active"><a href="#">...</a></li>
        <li><a href="#">...</a></li>
        <li class="disabled"><a href="#">...</a></li>
    </ul>
</div>


/* Breadcrumb */
<div class="breadcrumb">
    <ul>
        <li><a href="#">...</a></li>
    </ul>
</div>';
                echo htmlspecialchars($html);
                ?>
            </pre>

        <p>You can add the class <q>active</q> to select an element and <q>disabled</q> to disable an element.</p>
    </div>

    <div id="gridview">
        <div class="header">
            <h3>Layout</h3>
        </div>

        <p><em>Responsive flexible grid</em></p>

        <div class="typo">
            <strong>Default gridview (9 rows)</strong>
            <br/><br/>

            <br/><br/>

            <div class="grid">
                <div class="col9">
                    9
                </div>

                <div class="col1">
                    1
                </div>

                <div class="col8">
                    8
                </div>

                <div class="col2">
                    2
                </div>

                <div class="col7">
                    7
                </div>

                <div class="col3">
                    3
                </div>

                <div class="col6">
                    6
                </div>

                <div class="col4">
                    4
                </div>

                <div class="col5">
                    5
                </div>

                <div class="col5">
                    5
                </div>

                <div class="col4">
                    4
                </div>

                <div class="col6">
                    6
                </div>

                <div class="col3">
                    3
                </div>

                <div class="col7">
                    7
                </div>

                <div class="col2">
                    2
                </div>

                <div class="col8">
                    8
                </div>

                <div class="col1">
                    1
                </div>
            </div>
        </div>

        <br/>

        <div class="typo">
            <strong>Gridview with offset</strong>

            <br/><br/>

            <div class="grid">
                <div class="col1">
                    1
                </div>

                <div class="col2 offset6">
                    offset6
                </div>
            </div>
        </div>

        <pre class="example-pre">
                <?php
                $html = '
/* Grid */
<div class="grid">
    <div class="col1">...</div>
    <div class="col8">...</div>
</div>


/* Grid with offset of 6 */
<div class="grid">
    <div class="col1">...</div>
    <div class="col2 offset6">...</div>
</div>';
                echo htmlspecialchars($html);
                ?>
            </pre>

        <p>You can use 2 types of grids in addition of the default grid (<q>.grid-small</q> and
            <q>.grid-large</q>, with 6 and 12 rows).</p>
    </div>

    <div id="typography">
        <div class="header">
            <h3>Typography</h3>
        </div>

        <p class="center"><em>Headers, Lists, Paragraphs, Blockquotes, and other necessary elements</em></p>

        <div class="exemples inline">
            <h1>Title h1</h1>

            <h2>Title h2</h2>

            <h3>Title h3</h3>
            <h4>Title h4</h4>
            <h5>Title h5</h5>
            <h6>Title h6</h6>
        </div>

        <div class="exemples inline">
            <h1 class="sub">Subtitle h1</h1>

            <h2 class="sub">Subtitle h2</h2>
            <h3 class="sub">Subtitle h3</h3>
            <h4 class="sub">Subtitle h4</h4>
            <h5 class="sub">Subtitle h5</h5>
            <h6 class="sub">Subtitle h6</h6>
        </div>

        <div class="exemples inline">
            <h1>Title h1.
                <small>small header.</small>
            </h1>
            <h2>Title h2.
                <small>small header.</small>
            </h2>
            <h3>Title h3.
                <small>small header.</small>
            </h3>
            <h4>Title h4.
                <small>small header.</small>
            </h4>
            <h5>Title h5.
                <small>small header.</small>
            </h5>
            <h6>Title h6.
                <small>small header.</small>
            </h6>
        </div>

                <pre class="example-pre">
                <?php
                $html = '
/* Title */
<h1>...</h1>


/* Subtitle */
<h1 class="sub">...</h1>


/* Title + Subtitle */
<h1>...<small>...</small></h1>';
                echo htmlspecialchars($html);
                ?>
            </pre>

        <br/>

        <div class="exemples">
            <p class="justify">
                This is a paragraph. Paragraphs have margins, specific font size and line height to do not be difficult
                to read. In paragraphs, you can put in advance a
                <mark>word</mark>
                . Oops, it is
                <del>wrong</del>
                ! This is magic, nope? You can
                <ins>underline</ins>
                in using the <q>ins</q> or the couple
                &#60;u&#62;, &#60;/u&#62; tag. Earth <strong>turn around</strong> Moon. And it is important to know
                that the first man to walk on the Moon was <em>Armstrong</em>. Thank you the <abbr title="National
                Aeronautics and Space Administration">NASA</abbr>. If you want to copy this paragraph, select it and use
                <kbd>Ctrl</kbd> + <kbd>C</kbd>.
            </p>
        </div>

        <div class="exemples">
            <strong>Blockquotes</strong>
            <blockquote>With great power, comes great responsibility.<br/><span><em>Ben Parker in Spider-Man</em></span>
            </blockquote>
        </div>

        <div class="exemples">
            <strong>Code</strong>
            <code>Good! Gray <strong>with</strong> blue!</code>
        </div>

        <div class="exemples">
            <strong>Let's go coding</strong>

            <pre><span>public function alive($var) {</span>
<span>    if ($var == 1)</span>
<span>      return ("Great, you're alive");</span>
<span>    else</span>
<span>      return ("Oh shit, you're dead");</span>
<span>}</span></pre>
        </div>

        <div class="exemples">
            <strong>Lists are funny!</strong>
            <ul>
                <li class="square">List item with square</li>
                <li>List item with circle</li>
                <li class="none">Liste no decoration</li>
            </ul>
        </div>

        <div style="margin-bottom: 30px">
            <div class="jumbotron">
                <h4>Jumbotron heading</h4>

                <p>Cras justo odio, dapibus ac facilis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo,
                    tortor
                    maurais condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                <button class="btn btn-red">Button jumbotron</button>
            </div>
        </div>
    </div>

    <div id="tables">
        <div class="header">
            <h3>Tables</h3>
        </div>

        <p><em>All sorts of tables : default, alternate color, and mouse hover</em></p>

        <div class="exemple-table">
            <table>
                <tr>
                    <th>Header One</th>
                    <th>Header Two</th>
                </tr>
                <tr>
                    <td data-label="Data 1">Content Goes Here</td>
                    <td data-label="Data 2">Content Goes Here</td>
                </tr>
                <tr>
                    <td data-label="Data 1">Content Goes Here</td>
                    <td data-label="Data 2">Content Goes Here</td>
                </tr>
            </table>
        </div>

                        <pre class="example-pre">
                <?php
                $html = '
/* Default table */
<table>
    <tr>
        <th>...</th>
    </tr>

    <tr>
        /* Use data-label attribute for the responsive view */
        <td data-label="Data 1">...</td>
    </tr>
</table>';
                echo htmlspecialchars($html);
                ?>
            </pre>


        <br /><div class="exemple-table">
            <table class="alternate">
                <tr>
                    <th>Header One</th>
                    <th>Header Two</th>
                </tr>
                <tr>
                    <td data-label="Data 1">Content Goes Here</td>
                    <td data-label="Data 2">Content Goes Here</td>
                </tr>
                <tr>
                    <td data-label="Data 1">Content Goes Here</td>
                    <td data-label="Data 2">Content Goes Here</td>
                </tr>
            </table>
        </div>

        <pre class="example-pre">
        <?php
        $html = '
/* Alternate table */
<table class="alternate">
    <tr>
        <th>...</th>
    </tr>

    <tr>
        /* Use data-label attribute for the responsive view */
        <td data-label="Data 1">...</td>
    </tr>
</table>';
        echo htmlspecialchars($html);
        ?>
        </pre>

        <br /><div class="exemple-table">
            <table class="mouse">
                <tr>
                    <th>Header One</th>
                    <th>Header Two</th>
                </tr>
                <tr>
                    <td data-label="Data 1">Content Goes Here</td>
                    <td data-label="Data 2">Content Goes Here</td>
                </tr>
                <tr>
                    <td data-label="Data 1">Content Goes Here</td>
                    <td data-label="Data 2">Content Goes Here</td>
                </tr>
            </table>
        </div>

        <pre class="example-pre">
        <?php
        $html = '
/* Mouse hover table */
<table class="mouse">
    <tr>
        <th>...</th>
    </tr>

    <tr>
        /* Use data-label attribute for the responsive view */
        <td data-label="Data 1">...</td>
    </tr>
</table>';
        echo htmlspecialchars($html);
        ?>
        </pre>
    </div>

    <div id="buttons">
        <div class="header">
            <h3>Buttons</h3>
        </div>

        <p><em>Large, small, flat or round. Any width, any color</em></p>

        <div class="center exemple-table">
            <strong>Differents colors</strong>
            <br/><br/>
            <table class="alternate center">
                <tr>
                    <th>Button</th>
                    <th>Class=""</th>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn">Button</button>
                    </td>
                    <td data-label="Class">btn</td>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-blue">Button</button>
                    </td>
                    <td data-label="Class">btn-blue</td>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-red">Button</button>
                    </td>
                    <td data-label="Class">btn-red</td>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-brown">Button</button>
                    </td>
                    <td data-label="Class">btn-brown</td>
                </tr>
            </table>
        </div>

        <div class="center exemple-table">
            <br/><br/>
            <strong>Differents sizes</strong>
            <br/><br/>
            <table class="alternate center">
                <tr>
                    <th>Button</th>
                    <th>Class=""</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-small">Button</button>
                    </td>
                    <td data-label="Class">btn-small</td>
                    <td data-label="Description">Small</td>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn">Button</button>
                    </td>
                    <td data-label="Class">btn</td>
                    <td data-label="Description">Default</td>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-lg">Button</button>
                    </td>
                    <td data-label="Class">btn-lg</td>
                    <td data-label="Description">Large</td>
                </tr>
            </table>
        </div>

        <div class="center exemple-table">
            <strong>Differents types</strong>
            <br/><br/>
            <table class="alternate center">
                <tr>
                    <th>Button</th>
                    <th>Class=""</th>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-blue">Button</button>
                    </td>
                    <td data-label="Class">btn</td>
                </tr>
                <tr>
                    <td data-label="Button">
                        <button class="btn btn-blue round">Button</button>
                    </td>
                    <td data-label="Class">round</td>
                </tr>
            </table>
        </div>

        <div class="center">
            <br/><strong>Flat buttons</strong><br/>
            <button class="btn btn-red flat">Button</button>
            <button class="btn btn-brown flat">Button</button>
            <button class="btn btn-blue flat">Button</button>

         <pre class="example-pre">
        <?php
        $html = '
/* Flat button */
<button class="btn flat">...</button>';
        echo htmlspecialchars($html);
        ?>
        </pre>

            <br/><strong>Fancy buttons</strong><br/>
            <button class="btn btn-lg fancy">Button</button>

         <pre class="example-pre">
        <?php
        $html = '
/* Fancy button */
<button class="btn fancy">...</button>';
        echo htmlspecialchars($html);
        ?>
        </pre>

            <br/><br/><strong>Flat extra-large button</strong><br/>
            <button class="center btn btn-red flat extra" style="width: 50%">Button</button>

         <pre class="example-pre">
        <?php
        $html = '
/* Extra-large button */
<button class="btn extra">...</button>';
        echo htmlspecialchars($html);
        ?>
        </pre>
        </div>

    </div>

    <div id="alerts">
        <div class="header">
            <h3>Boxes</h3>
        </div>

        <p><em>Notify you!</em></p>

        <div class="alert-box">This is a information box!</div>

        <div class="alert-box success">This is a success box!</div>

        <div class="alert-box danger">This is a danger box!</div>

        <div class="alert-box warning">This is a warning box!</div>

        <pre class="example-pre">
        <?php
        $html = '
/* Boxes */
<div class="alert-box">This is a information box!</div>
<div class="alert-box success">This is a success box!</div>
<div class="alert-box danger">This is a danger box!</div>
<div class="alert-box warning">This is a warning box!</div>';

        echo htmlspecialchars($html);
        ?>
        </pre>
    </div>
</div>
</body>
</html>