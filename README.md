# Easy CSS #

Easy CSS is is a Sass/Stylus framework featuring style normalisation to semantic elements, typography and basic components. It's elegant, and it's the solution when you want to save your time. Easy was created as part of our courses at the Licence Professionnelle by MANGIN Romaric & LEGOUGNE Kévin.



### Informations ###
* Created by MANGIN Romaric & LEGOUGNE Kévin
* Bitbucket repository : https://bitbucket.org/mangin55u/framework-css
* HTML / PHP pages to have an overview of the features available : https://webetu.iutnc.univ-lorraine.fr/~mangin55u/framework-css/

 
### Installation ###
If you want to use Easy, one line is necessary in your HTML code

```
#!html

<link rel="stylesheet" type="text/css" href="css/easy.css">
```



### Browser support ###
* Google Chrome (latest)
* Mozilla Firefox (latest)
* Opera (latest)
* Microsoft Edge
* Internet Explorer 10+



### Notes ####
This README and the website listings all the features are in English. Indeed, an evolution and a rewriting of Easy is provided on Github to implement JavaScript and more composents. English is therefore appropriate.